import {ElementSelectionService} from './../../../../../app/element-selection.service';
import {ComponentInspectorService} from './../../../../../app/component-inspector.service';
import { Component, OnInit } from '@angular/core';

import { AccountService } from 'app/core/auth/account.service';
import { Session } from './session.model';
import { SessionsService } from './sessions.service';
import { Account } from 'app/core/user/account.model';

@Component({
  selector: 'jhi-sessions',
  templateUrl: './sessions.component.html',
})
export class SessionsComponent implements OnInit {
  account: Account | null = null;
  error = false;
  success = false;
  sessions: Session[] = [];

  constructor(public __elementSelectionService:ElementSelectionService, private __componentInspectorService:ComponentInspectorService,
private sessionsService: SessionsService, private accountService: AccountService) {this.__componentInspectorService.getComp(this);
}

  ngOnInit(): void {
    this.sessionsService.findAll().subscribe(sessions => (this.sessions = sessions));

    this.accountService.identity().subscribe(account => (this.account = account));
  }

  invalidate(series: string): void {
    this.error = false;
    this.success = false;

    this.sessionsService.delete(encodeURIComponent(series)).subscribe(
      () => {
        this.success = true;
        this.sessionsService.findAll().subscribe(sessions => (this.sessions = sessions));
      },
      () => (this.error = true)
    );
  }
}
