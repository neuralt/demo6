package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.Demo5App;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/**
 * Test class for the FooResource REST controller.
 *
 * @see FooResource
 */
@SpringBootTest(classes = Demo5App.class)
public class FooResourceIT {

    private MockMvc restMockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        FooResource fooResource = new FooResource();
        restMockMvc = MockMvcBuilders
            .standaloneSetup(fooResource)
            .build();
    }

    /**
     * Test fuckMe
     */
    @Test
    public void testFuckMe() throws Exception {
        restMockMvc.perform(get("/api/foo/fuck-me"))
            .andExpect(status().isOk());
    }

    /**
     * Test getMeSomething
     */
    @Test
    public void testGetMeSomething() throws Exception {
        restMockMvc.perform(get("/api/foo/get-me-something"))
            .andExpect(status().isOk());
    }
}
